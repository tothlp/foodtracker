# FoodTracker

It's a simple application for food rating, based on the iOS tutorial provided [here](https://developer.apple.com/library/archive/referencelibrary/GettingStarted/DevelopiOSAppsSwift/index.html#//apple_ref/doc/uid/TP40015214-CH2-SW1).
